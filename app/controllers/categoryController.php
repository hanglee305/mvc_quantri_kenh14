<?php 
$root = $_SERVER['DOCUMENT_ROOT']."/HocWebCB/MVC/quantri/";
require_once $root."/app/Models/CategoryModels.php";

 class categoryController{
    public function dashboard(){
        try{
            $list_category = categoryModel::getAll();
            $root = $_SERVER['DOCUMENT_ROOT']."/HocWebCB/MVC/quantri/";
            require_once $root."/app/views/CMS/pages/category/index.php";
        }
        catch(\Throwable $th){
         
            echo $th->getMessage();
        }
        
    }
    public function index(){
        try{
            $list_category = categoryModel::getAll();
            $root = $_SERVER['DOCUMENT_ROOT']."/HocWebCB/MVC/quantri/";
            require_once $root."/app/views/CMS/pages/category/index.php";
        }
        catch(\Throwable $th){
         
            echo $th->getMessage();
        }
        
    }
    public function create(){
        try{
            $list_category = categoryModel::getAll();
            $root = $_SERVER['DOCUMENT_ROOT']."/HocWebCB/MVC/quantri/";
            require_once $root."/app/views/CMS/pages/category/create.php";
        }
        catch(\Throwable $th){
         
            echo $th->getMessage();
        }
        
    }
    public function store($request){
        try{
            $new_category = new categoryModel(null,$request['name'],$request['id_parent']);
            $res = categoryModel::add($new_category);
            $response = [
               'status' => 'success',
               'message'=> 'Add successfully'          
           ];
           if(!$res){
               $response = [
                'status' => 'danger',
                'message'=> 'Add error'
               ];
           }
            $root = $_SERVER['DOCUMENT_ROOT']."/HocWebCB/MVC/quantri/";
            require_once $root."/app/views/CMS/pages/category/create.php";
        }
        catch(\Throwable $th){
         
            echo $th->getMessage();
        }
        
    }
    public function edit($id)
    {
        try{
            $root = $_SERVER['DOCUMENT_ROOT']."/HocWebCB/MVC/quantri/";
            $category = categoryModel::find($id);

            $list_category = categoryModel::getAll();

            if ($category == null){
                require_once $root ."/404.php";
            }
            require_once $root."/app/views/CMS/pages/category/edit.php";
        }
        catch(\Throwable $th){
         
            echo $th->getMessage();
        }
    }
    public function update($request,$id)
    {
    try{
        $root = $_SERVER['DOCUMENT_ROOT']."/HocWebCB/MVC/quantri/";
        $update_category = new categoryModel($id,$request['name'],$request['parent_id']);
        $res = categoryModel::update($category);
       
        $list_category = categoryModel::getAll();
        $response = [
            'status' => 'success',
            'message'=> 'Update successfully'          
        ];
        $category= categoryModel::find($id);
        if(!$res){
            $response = [
             'status' => 'danger',
             'message'=> 'Update error'
            ];
        }
        require_once $root."/app/views/CMS/pages/category/edit.php";
        }
        catch(\Throwable $th){
     
        echo $th->getMessage();
        }
    
    } 
    public function delete($id)
    {
    try{
        $res = categoryModel::delete($category);
        $response = [
            'status' => 'success',
            'message'=> 'Update successfully'          
        ];

        if(!$res){
            $response = [
             'status' => 'danger',
             'message'=> 'Update error'
            ];
        }
        header("location: http://localhost:8080/HocWebCB/MVC/quantri/category");
        }
        catch(\Throwable $th){
     
        echo $th->getMessage();
        }
    
    }
 }
?>