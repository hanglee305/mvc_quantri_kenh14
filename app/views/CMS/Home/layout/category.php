      
    <!-- CATEGORY-musik -->
    <section class="container">
        <div class="row">
            <!-- STAR -->
            <div class="col-lg-6">
                <div id="star">
                    <div class="pb-3">
                        <h3 class="border1 "><a href="category-musik.php"
                                class="text-decoration-none text-uppercase text-dark">star</a>
                        </h3>
                    </div>
                    <div>
                        <img src="https://kenh14cdn.com/thumb_w/620/2020/4/25/5882b2b7d0a20cf4f78955de7b094b36acaf9992-15878241656171216027619.jpg"
                            alt="" class="w-100 d-block shadow" height="300px">
                    </div>
                    <div>
                        <h4 class="py-2">
                            <a href="detail.php" class="text-title text-decoration-none">5 mối tình kín tiếng của
                                Triệu Vy: Toàn
                                nhân
                                vật “máu mặt” vừa có tiền vừa có quyền,
                                profile khủng đến mức khó ai bì kịp</a>
                        </h4>
                        <p>Triệu Vy trong" Hoàn Châu Cách Cách" và "Tân dòng sông ly biệt" đều có một cái kết viên mãn
                            với
                            những
                            người đàn ông có ...</p>
                    </div>
                    <ul class="list-unstyled">
                        <li><a href="detail.php" class="text-title text-decoration-none h6">Phan Mạnh Quỳnh bị
                                bạn gái "đột nhập"
                                Facebook để kể xấu: Lúc tán tỉnh nhiệt tình, ...
                            </a>
                        </li>
                        <li><a href="detail.php" class="text-title text-decoration-none h6">Bức ảnh năm 22 tuổi
                                chưa từng được
                                công bố
                                của Phạm Băng Băng bất ngờ gây bão dư luận, nhan sắc thật sự được hé lộ
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            <!-- END OF STAR -->
            <!-- TV SHOW -->
            <div class="col-lg-6">
                <div id="tvshow">
                    <div class="pb-3">
                        <h3 class="border1 "><a href="category-musik.php"
                                class="text-decoration-none text-uppercase text-dark">TV
                                show</a>
                        </h3>
                    </div>
                    <div>
                        <img src="https://kenh14cdn.com/thumb_w/620/2020/4/26/anh-1-15878683296912016755208.jpg" alt=""
                            class="w-100 d-block shadow" height="300px">
                    </div>
                    <div>
                        <h4 class="py-2">
                            <a href="detail.php" class="text-title text-decoration-none">Ngất ngây với tập đoàn
                                gái xinh tụ
                                hội cùng nhau "báo thù" trong "Thanh xuân có bạn"!</a>
                        </h4>
                        <p>Nhóm thí sinh này nhanh chóng "đánh cắp trái tim" người hâm mộ nhờ nhan sắc đẹp cả dàn
                            không chê vào đâu được ở tập 14 ...</p>
                    </div>
                    <ul class="list-unstyled">
                        <li class="py-2"><a href="detail.php" class="text-title text-decoration-none h6">Giữa
                                rừng mỹ nam an tĩnh
                                của Kpop
                                bất ngờ xuất hiện 1 "siêu mẫu" liên tục tạo dáng khác người!
                            </a>
                        </li>
                        <li class="py-2"><a href="detail.php" class="text-title text-decoration-none h6">Ngu Thư
                                Hân làm Triệu
                                Tiểu Đường
                                quê độ, khoe giọng hát ngọt ngào khiến fan kinh ngạc tại "Thanh xuân có bạn"
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            <!-- END OF TV SHOW -->
            <!-- CINE -->
            <div class="col-lg-6">
                <div id="cine">
                    <div class="pb-3">
                        <h3 class="border1 "><a href="category-musik.php"
                                class="text-decoration-none text-uppercase text-dark">cíne</a>
                        </h3>
                    </div>
                    <div>
                        <video
                            poster="https://kenh14cdn.com/2020/4/25/honeycam-2020-04-25-23-15-47-1587831358415246646714.gif.png"
                            src="https://kenh14cdn.com/2020/4/25/honeycam-2020-04-25-23-15-47-1587831358415246646714.gif.mp4"
                            alt="" style="width:100%;" preload="auto" loop="loop" height="300px"></video>
                    </div>
                    <div>
                        <h4 class="py-2">
                            <a href="detail.php" class="text-title text-decoration-none">Rating tập 4 Quân Vương
                                Bất Diệt tăng
                                không đủ "lết" lên 2 chữ số: Tình tiết quá chậm hay Lee Min Ho "hết thời"?</a>
                        </h4>
                        <p>Quân Vương Bất Diệt tập 4 ghi nhận mức rating tăng không đáng kể so với tập trước.</p>
                    </div>
                    <ul class="list-unstyled">
                        <li class="py-2"><a href="detail.php" class="text-title text-decoration-none h6">Review
                                "Time to Hunt":
                                Phiên bản "cướp giật" điển trai của Money Heist, ngoại hình 10 điểm nhưng "nội dung"
                                nhạt toẹt
                            </a>
                        </li>
                        <li class="py-2"><a href="detail.php" class="text-title text-decoration-none h6">Hội các
                                bà vợ quyền lực
                                của Thế Giới Hôn Nhân được thiết lập, những gã chồng tồi cứ chờ mà nếm mùi cay đắng
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- END OF CINE -->
            <!-  -->
            <div class="col-lg-6">
                <div id="musik">
                    <div class="pb-3">
                        <h3 class="border1 "><a href="category-musik.php"
                                class="text-decoration-none text-uppercase text-dark">musik</a>
                        </h3>
                    </div>
                    <div>
                        <div>
                            <img src="https://kenh14cdn.com/thumb_w/660/2020/4/25/mytam1-15878273389981924032213.jpg"
                                alt="" class="w-100 d-block shadow" height="300px">
                        </div>
                    </div>
                    <div>
                        <h4 class="py-2">
                            <a href="detail.php" class="text-title text-decoration-none">Đức Phúc tiết lộ ca khúc
                                của Mỹ Tâm mình
                                yêu thích nhất, Diệu Nhi thành thật khai báo là "fangirl" nhưng vẫn chưa đi dự concert
                                lần nào!</a>
                        </h4>
                        <p>Đức Phúc, Diệu Nhi hay cả Trịnh Tú Trung nghĩ gì nghe gì xem gì khi nhắc đến âm nhạc của Mỹ
                            Tâm?</p>
                    </div>
                    <ul class="list-unstyled">
                        <li class="py-2"><a href="detail.php" class="text-title text-decoration-none h6">Nam idol
                                quyết định bỏ
                                học nên bị bố đuổi khỏi nhà, liên tục thi trượt JYP và suýt mất suất ra mắt cùng nhóm
                                nhạc có vũ đạo đều tăm tắp
                            </a>
                        </li>
                        <li class="py-2"><a href="detail.php" class="text-title text-decoration-none h6">10 MV
                                Kpop được xem
                                nhiều nhất tuần: Kang Daniel, Zico, Solar (MAMAMOO) ồ ạt ra sản phẩm nhưng vẫn thua BTS,
                                BLACKPINK; nhà JYP lấy lại ngôi vương
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- END OF MUSIK -->
            <!--BEAUTY & FASHION -->
            <div class="col-lg-6">
                <div id="beauty-fashion">
                    <div class="pb-3">
                        <h3 class="border1 "><a href="category-musik.php"
                                class="text-decoration-none text-uppercase text-dark">beaty &
                                fashion</a>
                        </h3>
                    </div>
                    <div>
                        <div>
                            <img src="https://kenh14cdn.com/thumb_w/660/2020/4/25/photo-1-1587827566972146473186.jpg"
                                alt="" class="w-100 d-block shadow" height="300px">
                        </div>
                    </div>
                    <div>
                        <h4 class="py-2">
                            <a href="detail.php" class="text-title text-decoration-none">Quá nhanh quá nguy hiểm:
                                Nhờ 2 thứ
                                ngon-bổ-rẻ chưa đến 100k, mình đã có hàng mi dày hơn thấy rõ chỉ sau 3 ngày!</a>
                        </h4>
                        <p>Sau khi thử vô số loại dưỡng mi Hàn/Nhật đắt đỏ và thất bại, giờ tín đồ làm đẹp Trịnh Thu
                            Uyên mới tìm ra chân ái giúp cho hàng mi của mình.</p>
                    </div>
                    <ul class="list-unstyled">
                        <li class="py-2"><a href="detail.php" class="text-title text-decoration-none h6">Diện
                                cùng một chiếc
                                kính: Jennie sexy mà vẫn dễ thương, nữ thủ tướng của "Quân Vương Bất Diệt" lại sang
                                chảnh ngút ngàn
                            </a>
                        </li>
                        <li class="py-2"><a href="detail.php" class="text-title text-decoration-none h6">Chẳng
                                phải sữa rửa mặt
                                đắt đỏ, Á hậu Tường San chỉ dùng loại 190k để da mịn mướt không tỳ vết
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- END OF BEAUTY & FASHION -->
            <!--DOI-SONG  -->
            <div class="col-lg-6">
                <div id="doisong">
                    <div class="pb-3">
                        <h3 class="border1 "><a href="category-musik.php"
                                class="text-decoration-none text-uppercase text-dark">đời
                                sống</a>
                        </h3>
                    </div>
                    <div>
                        <div>
                            <img src="https://kenh14cdn.com/thumb_w/660/2020/4/26/942034112241587221988923126759307001987072n-1587880860734645078857.jpg"
                                alt="" class="w-100 d-block shadow" height="300px">
                        </div>
                    </div>
                    <div>
                        <h4 class="py-2">
                            <a href="detail.php" class="text-title text-decoration-none">Loạt hình chứng minh:
                                Khoảng cách xa
                                nhất trên đời này là món ăn tưởng tượng và thực tế khi hội gái đoảng vào bếp</a>
                        </h4>
                        <p>Không phải đường đến trái tim crush, con đường gập ghềnh và xa xôi nhất mà hội gái đoảng phải
                            đi chính là từ tưởng tượng đến thực tế.</p>
                    </div>
                    <ul class="list-unstyled">
                        <li class="py-2"><a href="detail.php" class="text-title text-decoration-none h6">"Mỹ nữ
                                200 pound" và màn
                                giảm cân ngoạn mục chứng minh con gái chỉ cần gầy ...
                            </a>
                        </li>
                        <li class="py-2"><a href="detail.php" class="text-title text-decoration-none h6">Gái Lào
                                từng học ở Việt
                                Nam khoe tài làm bánh, dân mạng xỉu lên xỉu xuống ...
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- END OF DOI-SONG -->
            <!--AN-QUAY-DI  -->
            <div class="col-lg-6">
                <div id="anquaydi">
                    <div class="pb-3">
                        <h3 class="border1 "><a href="category-musik.php"
                                class="text-decoration-none text-uppercase text-dark">ăn-quẩy-đi</a>
                        </h3>
                    </div>
                    <div>
                        <div>
                            <img src="https://kenh14cdn.com/thumb_w/620/2020/4/26/1-15878882359922058238353.png" alt=""
                                class="w-100 d-block shadow" height="300px">
                        </div>
                    </div>
                    <div>
                        <h4 class="py-2">
                            <a href="detail.php" class="text-title text-decoration-none">Sau clip đập hộp hàng
                                hiệu, công chúa
                                béo Quỳnh Anh - vợ Duy Mạnh bất ngờ "chuyển hướng" làm clip review ăn hàng</a>
                        </h4>
                        <p>Mới đây, Quỳnh Anh đã cùng với chị gái của mình thực hiện một clip đi ăn trong vòng 24h tại
                            Sài Gòn.</p>
                    </div>
                    <ul class="list-unstyled">
                        <li class="py-2"><a href="detail.php" class="text-title text-decoration-none h6">Năng lực
                                đặc biệt của
                                hội gái đoảng khi vào bếp: Từ món này “biến” ra món khác, nấu ăn mà cứ ngỡ như dùng
                                “phép thuật” (Phần 2)
                            </a>
                        </li>
                        <li class="py-2"><a href="detail.php" class="text-title text-decoration-none h6">Cuối
                                tuần đầu tiên sau
                                cách ly kèm combo trời trở lạnh: giới trẻ Hà Nội rủ nhau đi cafe "chém gió" nhưng chỉ
                                lác đác, không tập trung quá đông
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- END OF AN-QUAY-DI -->
            <!--XA HOI  -->
            <div class="col-lg-6">
                <div id="xahoi">
                    <div class="pb-3">
                        <h3 class="border1 "><a href="category-musik.php"
                                class="text-decoration-none text-uppercase text-dark">xã
                                hội</a>
                        </h3>
                    </div>
                    <div>
                        <div>
                            <img src="https://kenh14cdn.com/thumb_w/620/2020/4/26/photo-1-15878864378582006740067.png"
                                alt="" class="w-100 d-block shadow" height="300px">
                        </div>
                    </div>
                    <div>
                        <h4 class="py-2">
                            <a href="detail.php" class="text-title text-decoration-none">Người bán hàng rong, xe
                                ôm, bán vé số…
                                sẽ được nhận tiền từ gói hỗ trợ 62.000 tỷ đồng sau 12 ngày</a>
                        </h4>
                        <p>Sau khi gói hỗ trợ 62.000 tỷ được quyết định triển khai, theo quy trình, nhóm lao động tự do
                            gồm những người bán hàng rong, thu gom rác, bốc vác, xe ôm, bán vé số… sẽ được nhận tiền hỗ
                            trợ sau 12 ...</p>
                    </div>
                    <ul class="list-unstyled">
                        <li class="py-2"><a href="detail.php" class="text-title text-decoration-none h6">Cô gái
                                Đan Mạch từng đi
                                du lịch 5 tỉnh thành trước khi phát hiện nhiễm ...
                            </a>
                        </li>
                        <li class="py-2"><a href="detail.php" class="text-title text-decoration-none h6">Diễn
                                biến dịch ngày
                                26/4: Không có thêm ca mắc Covid-19, còn 45 bệnh nhân ...
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- END OF XA HOI -->
            <!--HOC DUONG  -->
            <div class="col-lg-6">
                <div id="hocduong">
                    <div class="pb-3">
                        <h3 class="border1 "><a href="category-musik.php"
                                class="text-decoration-none text-uppercase text-dark">học
                                đường</a>
                        </h3>
                    </div>
                    <div>
                        <div>
                            <img src="https://kenh14cdn.com/thumb_w/620/2020/4/26/photo-1-1587869810336360156060.jpg"
                                alt="" class="w-100 d-block shadow" height="300px">
                        </div>
                    </div>
                    <div>
                        <h4 class="py-2">
                            <a href="detail.php" class="text-title text-decoration-none">10 tuổi đã bị thúc ép
                                học như "thiên
                                tài" để vào đại học sớm, cha mẹ ngay sau đó đã phải hối hận về điều này!</a>
                        </h4>
                        <p>Một cô bé chỉ mới 10 tuổi nhưng đã vào đại học. Tưởng chừng điều này sẽ trở nên đáng tự hào
                            nhưng mọi chuyện lại trở nên phức tạp khi em đến trường</p>
                    </div>
                    <ul class="list-unstyled">
                        <li class="py-2"><a href="detail.php" class="text-title text-decoration-none h6">Việc nhẹ
                                lương cao dành
                                cho hội lười: Sát cánh 24/24 bên nghệ sĩ nổi ...
                            </a>
                        </li>
                        <li class="py-2"><a href="detail.php" class="text-title text-decoration-none h6">Ngày mai
                                27/4 đồng loạt
                                30 tỉnh thành sẽ đón học sinh quay trở lại trường
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- END OF HOC DUONG -->
            <!--THE GIOI  -->
            <div class="col-lg-6">
                <div id="thegioi">
                    <div class="pb-3">
                        <h3 class="border1 "><a href="category-musik.php"
                                class="text-decoration-none text-uppercase text-dark">thế
                                giới</a>
                        </h3>
                    </div>
                    <div>
                        <div>
                            <img src="https://cdnimg.vietnamplus.vn/t620/uploaded/hotnnz/2020_04_26/ttxvnhan_quoc5.jpg"
                                alt="" class="w-100 d-block shadow" height="300px">
                        </div>
                    </div>
                    <div>
                        <h4 class="py-2">
                            <a href="detail.php" class="text-title text-decoration-none">Diễn biến dịch Covid-19
                                thế giới tới
                                sáng ngày 26/4: Trên 2,9 triệu người nhiễm và 202.000 ca tử vong</a>
                        </h4>
                        <p>Tính tới 6h sáng 26/4, thống kê của trang worldometers.info cho thấy đã có 2.911.168 người
                            nhiễm Covid-19, trong đó 202.873 người tử vong.</p>
                    </div>
                    <ul class="list-unstyled">
                        <li class="py-2"><a href="detail.php" class="text-title text-decoration-none h6">Tình
                                hình dịch COVID-19
                                hết ngày 25/4 tại ASEAN: Toàn khối có gần 38.000 ...
                            </a>
                        </li>
                        <li class="py-2"><a href="detail.php" class="text-title text-decoration-none h6">Lỗ hổng
                                chết người trong
                                khu ổ chuột lớn nhất đất nước 1,3 tỷ dân: Cả ...
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- END OF THE GIOI -->
        </div>
    </section>
 