 
 <!-- footer -->
 <div style="background-color: #333" class="">
        <ul class="list-unstyled list-inline justify-content-center text-center" style="padding: 15px;">
            <li class="list-inline-item active text-center">
                <a class=" " href="/"><i class="fas fa-home text-danger"></i> </a>
            </li>
            <li class="list-inline-item">
                <a class="text-nav text-decoration-none " href="category-musik.php">Star</a>
            </li>
            <li class="list-inline-item">
                <a class="text-nav text-decoration-none" href="category-musik.php">TV Show</a>
            </li>
            <li class="list-inline-item">
                <a class="text-nav text-decoration-none" href="category-musik.php">Cíne</a>
            </li>
            <li class="list-inline-item">
                <a class="text-nav text-decoration-none" href="category-musik.php">Musik</a>
            </li>
            <li class="list-inline-item">
                <a class="text-nav text-decoration-none" href="category-musik.php">Beauty & Fashion</a>
            </li>
            <li class="list-inline-item">
                <a class="text-nav text-decoration-none" href="category-musik.php">Đời Sống</a>
            </li>
            <li class="list-inline-item">
                <a class="text-nav text-decoration-none" href="category-musik.php">Ăn-Quẩy-Đi</a>
            </li>
            <li class="list-inline-item">
                <a class="text-nav text-decoration-none" href="category-musik.php">Xã hội</a>
            </li>
            <li class="list-inline-item">
                <a class="text-nav text-decoration-none" href="category-musik.php">Học Đường</a>
            </li>
            <li class="list-inline-item">
                <a class="text-nav text-decoration-none" href="category-musik.php">Thế Giới</a>
            </li>
        </ul>
    </div>
 <section class="container" style="padding: 0;">
        <footer class="py-4 px-3">
            <div class="row">
                <div class="col-lg-6">
                    <div class="card mb-3 ">
                        <div class="row no-gutters">
                            <div class="col-md-4">
                                <img src="img/footer.jpg" class="card-img w-100 h-100" alt="...">
                            </div>
                            <div class="col-md-8">
                                <div class="card-body">
                                    <h5 class="card-title text-uppercase ">Đóng góp nội dung</h5>
                                    <p class="card-text"><i class="far fa-question-circle"></i> Câu hỏi thường gặp
                                    </p>
                                    <a class="card-text"><i class="far fa-envelope"></i> Bandoc@kenh14.vn</a>
                                    <p class="card-text"><small class="text-muted">Kenh14.vn rất hoan nghênh độc giả
                                            gửi
                                            thông
                                            tin và góp ý cho chúng tôi.</small></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="card mb-3 img-footer">
                        <div class="d-flex">
                            <img src="https://upload.wikimedia.org/wikipedia/commons/7/7e/Logo-Kenh14.jpg" alt=""
                                class=" d-inline" width="70px" height="70px">
                            <div class="mr-auto pl-3">
                                <h5 class="text-white">Kenh14.vn <i class="fas fa-check-circle text-primary"></i> </h5>
                                <p class="small text-white">5.634.302 lượt thích</p>
                            </div>
                        </div>
                        <div class=" d-flex justify-content-between banner-footer">
                            <div>
                                <a href="#" class="btn text-white d-block border border-footer " type="button"
                                    style="color: #333;">
                                    <i class="fab fa-facebook-square"></i> Thích
                                </a>
                            </div>
                            <div>
                                <a href="#" class="btn text-white d-block border border-footer" type="button">
                                    <i class="fas fa-envelope-square"></i> Gửi tin nhắn
                                </a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            </div>

            <div class="row pt-5">
                <div class="col-md-4 col-12 border-right">
                    <h5 class="text-uppercase text-dark">Trụ sở hà nội</h5>
                    <p class=" text-dark">Tầng 21, tòa nhà Center Building, Hapulico Complex, số 1 Nguyễn Huy
                        Tưởng, p. Thanh Xuân Trung, quận Thanh Xuân, Hà Nội. Điện thoại: 024 7309 5555, máy lẻ 370.
                    </p>
                    <p><i class="fas fa-map-marker-alt text-dark"></i><a href="#"
                            class="text-uppercase small text-dark text-decoration-none"> xem bản đồ</a></p>


                </div>
                <div class="col-md-4 col-12 border-right">
                    <h5 class="text-uppercase text-dark">chịu trách nhiệm quản lý nội dung</h5>
                    <div class="pl-3">
                        <p class="text-capitalize text-dark">bà nguyễn bích minh</p>
                    </div>
                    <h5 class="text-uppercase text-dark">hợp tác truyền thông</h5>
                    <div class="pl-3">
                        <p class="text-dark"><i class="fas fa-phone text-dark"></i> 024.73095555 (máy lẻ 62.370)</p>
                        <p class="text-dark"><i class="far fa-envelope text-dark"></i> marketing@kenh14.vn</p>
                    </div>
                    <h5 class="text-uppercase text-dark">liên hệ quảng cáo</h5>
                    <div class="pl-3">
                        <p class="text-dark"><i class="fas fa-phone text-dark"></i> 0942 86 11 33</p>
                        <p class="text-dark"><i class="far fa-envelope text-dark"></i> giaitrixahoi@admicro.vn</p>
                    </div>
                    <button class="btn btn-primary text-uppercase text-white d-block ">
                        <i class="fab fa-facebook-messenger"></i> chat vơi tư vấn viên
                    </button>
                    <button class="btn  text-uppercase text-white my-2 btn-cam" style="background-color: #d35400;">xem
                        chi tiết
                    </button>
                    <h5 class="text-uppercase text-dark">chính sách bảo mật</h5>
                </div>
                <div class="col-md-4 col-12 ">
                    <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcS2-5Egt2qSlXyMHMJkOKkj44Ab4rKu1HGxh6x-cwGh93poUdON&usqp=CAU"
                        alt="" class="" width="100px" height="50px">
                    <p class=" text-dark">&copy; Copyright 2007 - 2020 - <b>CÔNG TY CỔ PHẦN VCCORP</b></p>
                    <p class="small text-dark">Tầng 17, 19, 20, 21 Tòa nhà Center Building - Hapulico Complex, Số 1
                        Nguyễn Huy Tưởng, Thanh Xuân, Hà Nội.</p>
                    <p class="small text-dark">Giấy phép số 2215/GP-TTĐT do Sở Thông tin và Truyền thông Hà Nội cấp
                        ngày 10 tháng 4 năm 2019</p>
                </div>
            </div>


        </footer>
        <div class="h5 text-dark text-center py-2">&copy; 2020, Made By TH</div>
    </section>
    <!--end of footer  -->
    <script src="/js/javascript.js"></script>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
        crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
        crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/all.js"
        integrity="sha384-xymdQtn1n3lH2wcu0qhcdaOpQwyoarkgLVxC/wZ5q7h9gHtxICrpcaSUfygqZGOe"
        crossorigin="anonymous"></script>