<header>
        <nav class="navbar navbar-expand-md " style="background-color:  #fa983a;">
            <img src="img/Logo-Kenh14 1.png" alt="logo" class="logo">
            <div>
                <a class="navbar-brand d-block text-white h1" href="/">Kenh14.vn</a>
                <p class="text-white small">Kênh Giải Trí - Xã hội</p>
            </div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#myNavbar"
                aria-controls="myNavbar" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse  justify-content-md-center" id="myNavbar">
                <ul class="navbar-nav text-center">
                    <li class="list-inline active">
                        <a class="nav-link " href="/"><i class="fas fa-home text-dark fa-2x"></i> </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-nav " href="category-musik.php">Star</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-nav" href="category-musik.php">TV Show</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-nav" href="category-musik.php">Cíne</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-nav" href="category-musik.php">Musik</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-nav" href="category-musik.php">Beauty & Fashion</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-nav" href="category-musik.php">Đời Sống</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-nav" href="category-musik.php">Ăn-Quẩy-Đi</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-nav" href="category-musik.php">Xã hội</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-nav" href="category-musik.php">Học Đường</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-nav" href="category-musik.php">Thế Giới</a>
                    </li>
                </ul>
                <ul class="navbar-nav inboxx my-2 my-lg-0">
                    <li class="nav-item ml-md-auto il pl-5"><a href="part-bb/dangnhap.php" class="nav-link" data-toggle="modal"
                            data-target="#sign-out"><i class="fas fa-sign-out-alt text-danger fa-2x"></i></a></li>
                </ul>
            </div>
        </nav>
    </header>

    <!-- modal -->
    <div class="modal fade" id="sign-out">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Want to leave?</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    Press logout to leave
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" data-dismiss="modal">Stay Here</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">
                        <a href="part-bb/dangnhap.php" class="text-decoration-none text-white">Logout</a> </button>
                </div>
            </div>
        </div>
    </div>
    <!-- end of modal -->