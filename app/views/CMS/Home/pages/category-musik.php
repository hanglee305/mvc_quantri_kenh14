<!DOCTYPE html>
<html lang="en">

<head>
    <?php include('part-bb/header.php');?>
    <title> Star | Kenh14</title>
</head>

<body style="background-color: #F3F2F0;">
   <?php include('part-bb/nav.php');?>

<section class="container " >
<ul class="list-unstyled list-inline border-bottom py-3">
    <li class="list-inline-item active " >
        <a href="#" class="text-uppercase text-decoration-none h6 text-dark pb-3"id="border-detail" >musik</a>
    </li>
    <li class="list-inline-item">
        <a href="#" class="text-uppercase text-decoration-none h6 text-dark" >âu-mỹ</a>
    </li>
    <li class="list-inline-item">
        <a href="#" class="text-uppercase text-decoration-none h6 text-dark" >châu á</a>
    </li>
    <li class="list-inline-item">
        <a href="#" class="text-uppercase text-decoration-none h6 text-dark" >việt nam</a>
    </li>
</ul>
</section>
<!-- category- NOIBAT-->
<section class="container">
        <div class="row">
            <div class="col-lg-8 col-sm-8" style="padding: 0;">
                <div class="pb-5 ">
                    <div class="">
                        <img src="https://kenh14cdn.com/thumb_w/620/2020/4/26/lili-film-3-blackpink-lisa-dance-performance-video-15879019979331407118099.jpeg"
                            class="card-img-top w-100 mx-auto d-block shadow" height="450px" alt="photo">
                        <div class="card-body">
                            <h3 class="card-title font-weight-bold "><a href="detail.php"
                                    class="text-decoration-none  text-title">Fan Lisa (BLACKPINK) nổi giận đùng đùng khi idol bị tố đạo vũ đạo, mở hẳn project yêu cầu xin lỗi: Đừng động đến Lisa!</a> </h3>
                            <div class="d-flex">
                                <p class="text-uppercase d-inline "><span style="color: #e67e22;">pv,</span>
                                    theo trí
                                    thức
                                    trẻ
                                </p>
                                <p class="mr-auto px-3"><i class="far fa-clock"></i> 18:46 03/04/2020</p>
                            </div>
                            <p class="card-text">Người hâm mộ đang hết mực bảo vệ Lisa trước cáo buộc đạo vũ đạo.</p>
                            <button class="btn btn-sm btn-doc" style="background: #e67e22;"><a href="detail.php"
                                    class=" text-light text-decoration-none">Xem
                                    thêm.</a></button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-4" style="padding-left: 1rem;">
                <div class="pb-5 ">
                <ul class="list-unstyled">
                        <li class="">
                            <img src="https://kenh14cdn.com/thumb_w/620/2020/4/26/blackpink1npih-15879029678541308568414.jpg"
                                class="card-img-top w-100" height="150px" alt="photo">
                            <div class="card-body">
                                <h4 class="card-title font-weight-bold "><a href="detail.php"
                                        class="text-decoration-none text-title ">DJ Zedd tiết lộ đã có bài hát collab với BLACKPINK nhưng không thành, fan ...</a> </h4>
                            </div>
                        </li>
                        <li class="">
                            <img src="https://kenh14cdn.com/thumb_w/620/2020/4/26/13-26-e1587762652827-15879033105651804380614.jpg"
                                class="card-img-top w-100" height="150px" alt="photo">
                            <div class="card-body">
                                <h4 class="card-title font-weight-bold "><a href="detail.php"
                                        class="text-decoration-none text-title ">SuperM lần đầu trình diễn ca khúc mới toanh ngay tại concert online, hé lộ ...</a> </h4>
                            </div>
                        </li>
                    </ul>
                   
                </div>
            </div>
        </div>

    <?php include('part-bb/tinchuy.php');
    include('part-bb/category.php');
include('part-bb/footer.php');
    ?>
    </body>

</html>
