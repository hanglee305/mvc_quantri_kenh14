<!DOCTYPE html>
<html lang="en">

<head>
    <?php include('part-bb/header.php');?>
    <title>Kenh14</title>
</head>

<body style="background-color: #F3F2F0;">
    <?php include('part-bb/nav.php');?>

    <section class="container " id="tintuc">
        <div class="row pt-5">
            <!-- TINTUC -->
            <div class="col-lg-8  col-sm-8">
                <h2 class><b>Thêm 4 ca mắc Covid-19 mới, nâng tổng lên 237: 2 người từng đến bar Buddha, 1 ca đến
                        Việt
                        Nam từ tháng 12/2019 và đã đi rất nhiều nơi</b> </h2>
                <div>
                    <div class="d-flex">
                        <p class="text-uppercase d-inline "><span class="" style="color: #e67e22;">pv,</span> theo
                            trí
                            thức
                            trẻ
                        </p>
                        <p class="mr-auto px-3"><i class="far fa-clock"></i> 18:46 03/04/2020</p>
                    </div>
                    <div class="d-flex pb-3">
                        <div class=" ">
                            <button class="btn btn-primary btn-sm small"><i class="fab fa-facebook-f"></i>
                                <span>Chia sẻ
                                    258</span></button>
                            <button class="btn btn-sm btn-muted border text-light"
                                style="background-color: #0c2461;"><span><i class="fas fa-bookmark text-light"></i>
                                    Lưu
                                    vào
                                    Facebook</span></button>
                        </div>
                        <div class=" ml-auto">
                            <button class="btn btn-sm border  border-radius" style="background-color: black;"><i
                                    class="far fa-envelope text-white"></i></button>
                            <button class="btn btn-sm border btn-primary border-radius"><i
                                    class="fab fa-facebook-messenger text-white"></i> Gửi</button>
                        </div>
                    </div>
                </div>
                <div class="underline-detail text-muted my-2"></div>
                <div class="row">
                    <div class="col-1 ">
                        <button style="background-color:#e67e22;" class=" btn rounded-circle my-2  btn-icon"><i
                                class="fas fa-home text-white "></i>
                        </button>
                        <button class="btn border-radius rounded-circle  btn-icon" style="background-color: #0c2461;"><i
                                class="fab fa-facebook-f text-white "></i></button>
                    </div>
                    <div class="col-11">
                        <h5 class="py-2"><b>18h30 ngày 3/4, Bộ Y tế chính thức công bố thêm 4 ca bệnh COVID-19 mới,
                                nâng
                                tổng số mắc
                                bệnh này ở Việt Nam lên 237 ca.</b></h5>
                        <ul>
                            <li><a href="part/detail-musik.php" class="text-decoration-none text-muted small">Hà Nội:
                                    Cảnh
                                    sát
                                    kiên
                                    nhẫn thuyết phục cô gái định trốn khỏi chung cư cách ly nơi có 2 vợ chồng dương
                                    tính
                                    lần 1 với Covid-19 </a> </li>
                            <li><a href="part/detail-musik.php" class="text-decoration-none text-muted small">Chủ tịch
                                    Hà
                                    Nội:
                                    Người dân phải tự rà soát lại trong những ngày qua, tiếp xúc với ai cũng phải
                                    nghĩ
                                    đến hai chữ "Bạch Mai" </a> </li>
                            <li><a href="part/detail-musik.php" class="text-decoration-none text-muted small">Cảnh đìu
                                    hiu
                                    chưa
                                    từng có ở Hải Phòng, cả bến xe chỉ phục vụ 1 khách đường dài </a> </li>
                        </ul>
                        <a href="https://kenh14cdn.com/2020/4/3/benhvienphusantwchongdich-15859141291351602889794.jpg"
                            class="image-popup-fit-width">
                            <img src="https://kenh14cdn.com/2020/4/3/benhvienphusantwchongdich-15859141291351602889794.jpg"
                                alt="anh" class="w-100">
                        </a>
                        <h5 class="py-3">Cụ thể:</h5>
                        <p class="text-body"><b>CA BỆNH 234 (BN234):</b> Bệnh nhân nam, 25 tuổi, quốc tịch Anh. Ngày
                            14/3/2020 có đi quán
                            Bar Buddha (TP. Hồ Chí Minh). Bệnh nhân không có triệu chứng, được cách ly tập trung tại
                            Quận 9. Mẫu bệnh phẩm từ Bệnh viện Quận 9, TP. Hồ Chí Minh gửi Bệnh viện Bệnh Nhiệt đới
                            TP.
                            Hồ Chí Minh cho kết dương tính với SARS-CoV-2. Ngay sau khi có kết quả xét nghiệm, bệnh
                            nhân
                            được chuyển đi cách ly và điều trị tại Bệnh viện Dã chiến Củ Chi .</p>

                        <p class="text-body"><b>CA BỆNH 235 (BN235):</b> Bệnh nhân nữ, 69 tuổi, quốc tịch Việt Nam,
                            có
                            địa chỉ tại
                            huyện Buôn Đôn, tỉnh Đăk Lăk. Ngày 17/3, bệnh nhân từ Paris về Việt Nam trên chuyến bay
                            của
                            Vietnam Airlines, số hiệu VN2106, số ghế 45 và nhập cảnh tại Sân bay Tân Sơn Nhất ngày
                            18/3.
                            Sau khi nhập cảnh, bệnh nhân được đưa đi cách ly tập trung tại Trường Quân sự Quân khu
                            7,
                            TP. Hồ Chí Minh. <br>Trong quá trình cách ly, bệnh nhân được xét nghiệm lần 1 cho kết
                            quả âm
                            tính và có sức khỏe ổn định. Ngày 30/3, bệnh nhân được lấy mẫu xét nghiệm lần 2 và cho
                            kết
                            quả dương tính với virus SARS-CoV-2. Hiện bệnh nhân được chuyển đến cách ly và theo dõi
                            tại
                            Bệnh viện Điều trị COVID-19 Cần Giờ.</p>

                        <p class="text-body"><b>CA BỆNH 236 (BN236):</b> Bệnh nhân nữ, 26 tuổi, quốc tịch Anh, địa
                            chỉ
                            lưu trú tại phường
                            Thảo Điền, Quận 2, TP. Hồ Chí Minh. Ngày14/3, bệnh nhân đi quán bar Buddha, tiếp xúc với
                            trường hợp bệnh COVID-19. Từ ngày 25/3, bệnh nhân được cách ly tập trung tại Học viện
                            Chính
                            trị Quận 9. Kết quả xét nghiệm lần 1 ngày 29/3/2020: không xác định; được lấy mẫu lần 2
                            ngày
                            01/4/2020 cho kết quả dương tính với SARS-COV-2. Ngay sau khi có kết quả xét nghiệm,
                            bệnh
                            nhân được chuyển đi cách ly và điều trị tại Bệnh viện Dã chiến Củ Chi.</p>

                        <p class="text-body"><b>CA BỆNH 237 (BN237): </b>Bệnh nhân nam,64 tuổi, quốc tịch Thụy Điển,
                            đến
                            Việt Nam từ cuối
                            tháng 12/2019, di chuyển nhiều địa điểm như: Ninh Bình (17/3), quay lại Hà Nội từ 22/3
                            đến
                            nay. Bệnh nhân bị ung thư máu (bạch cầu cấp).</p>

                        <p class="text-body">Ngày 26/3, bệnh nhân bị tai nạn và được chở vào Bệnh viện Việt Pháp
                            bằng xe
                            cấp cứu 115, sau
                            đó quay lại khách sạn. Ngày 31/3, bệnh nhân bị chảy máu mũi nhiều, được người nhà đưa
                            sang
                            Bệnh viện Đa khoa Đức Giang, được khám và chuyển đến Viện Huyết học và Truyền máu Trung
                            ương.<br>Sáng 1/4, bệnh nhân được lấy mẫu bệnh phẩm và xét nghiệm cho kết quả dương tính
                            với
                            SARS-CoV-2. Bệnh nhân được chuyển đến cách ly và điều trị tại Bệnh viện Bệnh Nhiệt đới
                            Trung
                            ương cơ sở 2.<br>Như vậy, tính đến thời điểm này, Việt Nam đã ghi nhận 237 ca COVID-19,
                            trong đó có 85 trường hợp đã được công bố khỏi bệnh. Các bệnh nhân còn lại hiện đang
                            được
                            cách ly, điều trị tại các cơ sở y tế trong cả nước. Đa số bệnh nhân có tình trạng sức
                            khỏe
                            ổn định.</p>
                    </div>
                </div>
                <div class="underline-detail text-muted my-2"></div>
                <div class="py-2 d-flex ">
                    <button class="btn btn-sm border btn-primary border-radius px-2"><i
                            class="fab fa-facebook-messenger text-white"></i> Gửi</button>
                    <button class="btn btn-primary btn-sm small mx-2 text-light" style="background-color: #0c2461;"><i
                            class="fab fa-facebook-f"></i> <span>Chia sẻ
                            258</span></button>
                    <button class="btn btn-primary btn-sm small"><i class="far fa-thumbs-up"></i> <span>
                            Thích</span></button>
                    <button class="btn  btn-sm small ml-auto text-white" style="background-color: #48dbfb;"><i
                            class="fas fa-eye text-white"></i> Tăng View<span>
                        </span></button>
                </div>
                <div class="underline-detail text-muted my-2"></div>
                <button class="btn border m-1 btn-tag-bgr btn-outline-dark" type="button " data-toggle="tooltip"
                    data-placement="bottom" title="tin nóng xã hội"><a href="#"
                        class="text-decoration-none btn-tag ">#tin nóng xã hội</a> </button>
                <button class="btn border m-1 btn-tag-bgr btn-outline-dark" type="button " data-toggle="tooltip"
                    data-placement="bottom" title="Covid-19 là đại dịch toàn
                cầu"><a href="#" class="text-decoration-none btn-tag "> #Covid-19 là đại dịch toàn
                        cầu</a></button>
                <button class="btn border m-1 btn-tag-bgr btn-outline-dark" type="button " data-toggle="tooltip"
                    data-placement="bottom" title="Các ca nhiễm Covid-19 tại
                Việt Nam"><a href="#" class="text-decoration-none btn-tag">#Các ca nhiễm Covid-19 tại
                        Việt Nam</a></button>
                <button class="btn border m-1 btn-tag-bgr btn-outline-dark" type="button " data-toggle="tooltip"
                    data-placement="bottom" title="ICT_anti_nCoV"><a href="#" class="text-decoration-none btn-tag">
                        #ICT_anti_nCoV</a></button>
                <button class="btn border m-1 btn-outline-dark btn-tag-bgr" type="button " data-toggle="tooltip"
                    data-placement="bottom" title="Diễn biến dịch Covid-19 tại
                Việt Nam"><a href="#" class="text-decoration-none btn-tag">#Diễn biến dịch Covid-19 tại
                        Việt Nam</a>
                </button>
            </div>
            <div class="col-lg-4 col-sm-4 pt-5">
                <h5 class="text-uppercase text-white border p-2" style="background-color: #e67e22;">tin tương tự</h5>
                <ul class="list-unstyled">
                    <li class="row py-2">
                        <div class="col-lg-5 col-sm-6">
                            <img src="https://kenh14cdn.com/thumb_w/620/2020/4/27/photo-1-1587956345567307257936.jpg"
                                alt="" class="w-100 d-block shadow" height="150px">
                        </div>
                        <div class="col-lg-7 col-sm-6">

                            <h6><a href="detail.php" class=" text-title text-decoration-none">994 mẫu xét nghiệm liên quan đến bệnh nhân 268 đều âm tính với virus gây COVID-19</a>
                            </h6>

                            
                            <p class="small">Liên quan đến trường hợp bệnh tại huyện Đồng Văn, tỉnh Hà Giang, cơ quan chức năng đã thực hiện xét nghiệm nhanh (kháng ...</p>
                            
                        </div>
                    </li>
                    <li class="row py-2">
                        <div class="col-lg-5 col-sm-6">
                            <img src="https://kenh14cdn.com/thumb_w/620/2020/4/27/photo1587954768157-1587954768417-crop-15879547832361715809634-1587958763094198398894.jpg"
                                alt="" class="w-100 d-block shadow" height="150px">
                        </div>
                        <div class="col-lg-7 col-sm-6">

                            <h6><a href="detail.php" class=" text-title text-decoration-none">Nghi phạm nổ súng cướp ngân hàng ở Hà Nội đã đầu thú </a> </h6>

                            
                            <p class="small">Qua điều tra, sàng lọc, công an đã xác định được nghi phạm gây ra vụ cướp là người ở Uông Bí, Quảng Ninh. Sau đó, tên ...</p>
                            
                        </div>
                    </li>
                    <li class="row py-2">
                        <div class="col-lg-5 col-sm-6">
                            <img src="https://kenh14cdn.com/thumb_w/620/2020/4/27/photo-1-15879568624851078248122.jpg"
                                alt="" class="w-100 d-block shadow" height="150px">
                        </div>
                        <div class="col-lg-7 col-sm-6">

                            <h6><a href="detail.php" class=" text-title text-decoration-none">Từ 4/5, BV Bạch Mai tiếp nhận bệnh nhân BHYT thuộc chương trình quản lý bệnh mạn tính</a>
                            </h6>

                            
                            <p class="small">Sáng 27/4, BV Bạch Mai cho biết, bắt đầu từ ngày 4/5/2020 Khoa Khám bệnh của BV sẽ tiếp nhận và khám những bệnh nhân ...</p>
                            
                        </div>
                    </li>
                    <li class="row py-2">
                        <div class="col-lg-5 col-sm-6">
                            <img src="https://kenh14cdn.com/thumb_w/620/2020/4/27/photo-1-15879583192811000959276.jpg"
                                alt="" class="w-100 d-block shadow" height="150px">
                        </div>
                        <div class="col-lg-7 col-sm-6">

                            <h6><a href="detail.php" class=" text-title text-decoration-none">Sau văn bản của Cục Hàng không, Vietjet Air thông báo hoàn tiền trong vòng 90 ngày cho khách hàng có chuyến bay bị ảnh hưởng bởi Covid-19</a>
                            </h6>

                            
                            <p class="small">Khách hàng của Vietjet Air giờ đây có 3 lựa chọn: Đổi thời gian bay miễn phí sang giai đoạn từ 24/4 đến 31/5/2020 ...</p>
                            
                        </div>
                    </li>
                    <li class="row py-2">
                        <div class="col-lg-5 col-sm-6">
                            <img src="https://kenh14cdn.com/thumb_w/620/2020/4/27/photo-1-1587954028707335215803.jpg"
                                alt="" class="w-100 d-block shadow" height="150px">
                        </div>
                        <div class="col-lg-7 col-sm-6">

                            <h6><a href="detail.php" class=" text-title text-decoration-none">Xe máy tông trụ đèn rồi văng hơn 50m lên vỉa hè, đôi nam nữ chết thảm</a>
                            </h6>

                            
                            <p class="small">Một đôi nam nữ đã tử vong thương tâm sau khi đi xe máy tông vào trụ đèn, xe máy văng hơn 50m lên vỉa hè trước cửa nhà ...</p>
                            
                        </div>
                    </li>
                    <li class="row py-2">
                        <div class="col-lg-5 col-sm-6">
                            <img src="https://kenh14cdn.com/thumb_w/620/2020/4/27/9431292714336935501487585654624281145901056n-1587955829644541002652.jpg"
                                alt="" class="w-100 d-block shadow" height="150px">
                        </div>
                        <div class="col-lg-7 col-sm-6">

                            <h6><a href="detail.php" class=" text-title text-decoration-none">Hà Nội: Ô tô và xe máy va chạm, hai bên lao vào ẩu đả dẫn đến thương tích</a>
                            </h6>

                            
                            <p class="small">Sau va chạm giao thông giữa xe ô tô và xe máy, cả hai bên bất ngờ xảy ra ẩu đả dẫn đến thương tích. Hiện vụ việc đang ...</p>
                            
                        </div>
                    </li>
                    <li class="row py-2">
                        <div class="col-lg-5 col-sm-6">
                            <img src="https://kenh14cdn.com/thumb_w/620/2020/4/27/crop-15879558151342081765181.png"
                                alt="" class="w-100 d-block shadow" height="150px">
                        </div>
                        <div class="col-lg-7 col-sm-6">

                            <h6><a href="detail.php" class=" text-title text-decoration-none">Hà Nội: Phẫn nộ xe ô tô tông vào bé gái đi xe đạp điện rồi kéo lê xe hàng chục km</a>
                            </h6>

                            
                            <p class="small">Sau khi gây tai nạn với một cháu bé đi xe đạp điện, chiếc xe ô tô CX5 mang biển số liên doanh đã tiếp tục nhấn ga bỏ ...</p>
                            
                        </div>
                    </li>
                    <li class="row py-2">
                        <div class="col-lg-5 col-sm-6">
                            <img src="https://kenh14cdn.com/thumb_w/620/2020/4/27/20200422095804-15879536260411555767212.jpg"
                                alt="" class="w-100 d-block shadow" height="150px">
                        </div>
                        <div class="col-lg-7 col-sm-6">

                            <h6><a href="detail.php" class=" text-title text-decoration-none">TP.HCM: 2 bệnh nhân liên quan đến bar Buddha dương tính trở lại với SARS-COV-2 sau nhiều ngày được công bố khỏi bệnh</a>
                            </h6>

                            
                            <p class="small">Theo đó, cả 2 ca nhiễm Covid-19 dương tính trở lại đều liên quan đến bar Buddha, hiện được đưa vào BV Dã chiến Củ Chi ...</p>
                            
                        </div>
                    </li>
                    
                </ul>
            </div>
        </div>
    </section>
    <!-- END OF TINTUC -->
    <?php 
           include ('part-bb/category.php');
    ?>
    <?php   include('part-bb/footer.php');?>
</body>

</html>