<?php

include dirname(__DIR__)."/Models/DBConnect.php"; 

class categoryModel extends DBconnect
{
   var $id;
   var $name;
   var $id_parent;
   public function __construct($id, $name, $id_parent)
   {
      $this->id = $id;
      $this->name = $name;
      $this->id_parent = $id_parent;
   }
   static public function add($category)
   {
      $db = static::connect();
      $sql = "INSERT INTO categories ( name, id_parent) VALUES ('" . $category->name . "'," . $category->id_parent . ")";

      $result = true;
      if ($db->query($sql) === TRUE) {
         $result = true;
      } else {
         $result = false;
         throw new exception("Error: " . $sql . "<br>" . $db->error);
      }
      return $result;
   }

   static public function update($category)
   {
      $db = static::connect();
      $sql =  "UPDATE categories SET 'name'='" . $category->name . "', 'id_parent'= " . $category->id_parent . " WHERE 'id'=.$category->id.";
      $result = true;
      if ($db->query($sql) === TRUE) {
         $result = true;
      } else {
         $result = false;
         throw new exception("Error: " . $sql . "<br>" . $db->error);
      }
      return $result;
   }

   static public function delete($id)
   {
      $db = static::connect();
      $sql = "DELETE FROM categories WHERE 'id'=$id";

      $result = true;
      if ($db->query($sql) === TRUE) {
         $result = true;
      } else {
         $result = false;
         throw new exception("Error: " . $sql . "<br>" . $db->error);
      }
      return $result;
   }
   static public function getAll()
   {
      $db = static::connect();
      $sql = "SELECT * FROM categories";
      $result = $db->query($sql);
      $li = [];
  
      if ($result->num_rows > 0) {
         // output data of each row
         while ($row = $result->fetch_assoc()) {
            $li[] = new categoryModel($row['id'], $row['name'], $row['id_parent']);
         }
      } 
      $db->close();

      return $li;
   }
   
   static public function find($id)
   {
      $db = static::connect();
      $sql = "SELECT * FROM categories WHERE'id'=$id";
      $result = $db->query($sql);
      $data = null;
      if (!$result) {
         trigger_error('Invalid query: ' . $db->error);
     }
      if ($result->num_rows > 0) {

         // output data of each row
         while ($row = $result->fetch_assoc()) {
            $data[] = new categoryModel($row['id'], $row['name'], $row['id_parent']);
         }
      } 
      $db->close();

      return $data;
   }
}
?>